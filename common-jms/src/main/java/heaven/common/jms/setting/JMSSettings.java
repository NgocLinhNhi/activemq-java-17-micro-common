package heaven.common.jms.setting;

import java.util.HashMap;
import java.util.Map;

public class JMSSettings {

    protected final Map<Object, JMSEndpointSetting> endpointSettings;

    public JMSSettings() {
        this.endpointSettings = new HashMap<>();
    }

    public JMSEndpointSetting getEndpointSetting(Object key) {
        return endpointSettings.get(key);
    }

    public Map<Object, JMSEndpointSetting> getEndpointSettings() {
        return new HashMap<>(endpointSettings);
    }

    public JMSSettings addEndpointSetting(JMSEndpointSetting setting) {
        endpointSettings.put(setting.getId(), setting);
        return this;
    }

}
