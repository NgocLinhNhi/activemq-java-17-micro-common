package heaven.common.jms;

import heaven.common.jms.constant.JMSConstants;

import javax.jms.Connection;
import javax.jms.QueueConnection;
import javax.jms.TopicConnection;
import java.util.Properties;

public class JMSEndpoint {

    protected final String id;
    protected final String name;
    protected final Properties properties;

    public JMSEndpoint(Properties properties) {
        this.properties = new Properties(properties);
        this.id = properties.getProperty(JMSConstants.ID_KEY);
        this.name = properties.getProperty(JMSConstants.NAME_KEY);
    }

    public Connection createConnection() {
        return JMSConnectionFactoryProxy.getInstance().createConnection();
    }

    public QueueConnection createQueueConnection() {
        return JMSConnectionFactoryProxy.getInstance().createQueueConnection();
    }

    public TopicConnection createTopicConnection() {
        return JMSConnectionFactoryProxy.getInstance().createTopicConnection();
    }

    public void connectToJMSServer() throws Exception {
    }
}
