package heaven.common.jms.receiver;

import heaven.common.jms.JMSTopic;

import javax.jms.JMSException;
import java.util.Properties;

public class JMSTopicSubscriber extends JMSTopic implements Runnable {

    public JMSTopicSubscriber(Properties properties) {
        super(properties);
        this.topicMode = TOPIC_MODE_SUBSCRIBER;
    }

    @Override
    public void run() {
        goSubscribe();
    }

    private void goSubscribe() {
        try {
            while (topicOpened && !Thread.interrupted()) {
                subscribeMsg();
            }
        } catch (JMSException e) {
            onException(e);
        } finally {
            doFinally();
        }

    }

    protected void subscribeMsg() throws JMSException {
    }

    protected void doFinally() {
    }

    protected void onException(Exception e) {
    }

}
