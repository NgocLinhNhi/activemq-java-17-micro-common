package heaven.common.jms.receiver;

import heaven.common.jms.JMSQueue;

import javax.jms.JMSException;
import java.util.Properties;

public class JMSQueueSubscriber extends JMSQueue implements Runnable {

    public JMSQueueSubscriber(Properties properties) {
        super(properties);
        this.queueMode = QUEUE_MODE_RECEIVER;
    }

    @Override
    public void run() {
        goReceive();
    }

    private void goReceive() {
        try {
            while (queueOpened && !Thread.interrupted()) {
                receiveMsg();
            }
        } catch (JMSException e) {
            onException(e);
        } finally {
            doFinally();
        }

    }

    protected void doFinally() {
    }

    protected void receiveMsg() throws JMSException {
    }

    protected void onException(Exception e) {
    }
}
