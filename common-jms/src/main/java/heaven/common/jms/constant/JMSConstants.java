package heaven.common.jms.constant;

public final class JMSConstants {

    public static final int RESPONSE_TYPE_QUEUE = 1;
    public static final int RESPONSE_TYPE_TOPIC = 2;
    public static final String ID_KEY = "id";
    public static final String NAME_KEY = "name";
    public static final String RESPONSE_TO_KEY = "responseTo";
    public static final String RESPONSE_TYPE_KEY = "responseType";
    public static final String MAX_CAPACITY_KEY = "maxCapacity";

    private JMSConstants() {}

}
