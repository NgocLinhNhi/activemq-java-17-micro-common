package heaven.common.jms.exception;

public class JMSUnsupportMessageTypeException extends IllegalArgumentException {

    public JMSUnsupportMessageTypeException(Class<?> messageType) {
        super("unsupport message type: " + messageType.getTypeName());
    }

}
