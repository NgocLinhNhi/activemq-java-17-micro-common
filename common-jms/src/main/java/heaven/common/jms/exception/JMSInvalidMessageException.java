package heaven.common.jms.exception;

public class JMSInvalidMessageException extends IllegalArgumentException {

    public JMSInvalidMessageException(String msg, Exception e) {
        super(msg, e);
    }

}
