package heaven.common.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.Properties;

public abstract class JMSQueue extends JMSEndpoint {

    protected Queue queue;
    protected QueueSession session;
    protected QueueSender sender;
    protected QueueReceiver receiver;
    protected QueueConnection connection;

    protected byte queueMode;
    protected volatile boolean queueOpened = true;

    public final static byte QUE_MODE_SENDER = 0;
    public final static byte QUEUE_MODE_RECEIVER = 1;

    Logger logger = LoggerFactory.getLogger(JMSQueue.class);

    public JMSQueue(Properties properties) {
        super(properties);
    }

    @Override
    //Về cơ bản vẫn là mỗi cái message đều tạo ra Queue/Topic connection => config nông dân như 2 project kia là ok rồi
    //=> chỉ là không có dùng thread để xử lý message
    public void connectToJMSServer() throws Exception {
        connection = createQueueConnection();
        session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
        queue = session.createQueue(name);

        if (queueMode == QUE_MODE_SENDER) {
            sender = session.createSender(queue);
            sender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        } else {
            receiver = session.createReceiver(queue);
        }

        createMoreComponents();

        connection.start();
    }

    protected void createMoreComponents() throws Exception {
    }

    public void closeJMSQueue() throws JMSException {
        queueOpened = false;

        closeReceiver();
        closeSender();
        closeSession();
        closeConnection();

        queue = null;
    }

    public void closeSession() throws JMSException {
        if (session != null) {
            session.close();
            session = null;
        }
    }

    public void closeConnection() throws JMSException {
        if (connection != null) {
            connection.close();
            connection = null;
        }
    }

    public void closeReceiver() throws JMSException {
        if (receiver != null) {
            receiver.close();
            receiver = null;
        }
    }

    public void closeSender() throws JMSException {
        if (sender != null) {
            sender.close();
            sender = null;
        }
    }

    public void forceClose() {
        try {
            closeJMSQueue();
        } catch (Exception e) {
            logger.error("close jms queue error", e);
        }
    }
}
