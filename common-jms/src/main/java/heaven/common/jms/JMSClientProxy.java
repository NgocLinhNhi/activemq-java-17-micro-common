package heaven.common.jms;

import heaven.common.jms.concurrent.NThreadFactory;
import heaven.common.jms.setting.JMSEndpointSetting;
import heaven.common.jms.setting.JMSSettings;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.function.Function;

public class JMSClientProxy {

    protected final JMSSettings settings;
    protected final Map<String, JMSEndpoint> endpointByIds;
    @SuppressWarnings("rawtypes")
    protected final Map<Class, JMSEndpoint> endpointByTypes;

    public JMSClientProxy(JMSSettings settings) {
        this.settings = settings;
        this.endpointByIds = new HashMap<>();
        this.endpointByTypes = new HashMap<>();
    }

    public void start() {
        Map<Object, JMSEndpointSetting> endpointSettings = settings.getEndpointSettings();
        for (Object key : endpointSettings.keySet()) {
            JMSEndpointSetting setting = endpointSettings.get(key);
            startEndpoint(setting);
        }
    }

    //đăng ký tạo mới queue/topic cho hệ thống
    protected void startEndpoint(JMSEndpointSetting setting) {
        Function<Properties, JMSEndpoint> supplier = setting.getSupplier();
        JMSEndpoint endpoint = supplier.apply(setting.toProperties());
        try {
            endpoint.connectToJMSServer();
        } catch (Exception e) {
            throw new IllegalStateException("can't start endpoint: " + setting.getId(), e);
        }
        endpointByIds.put(setting.getId(), endpoint);
        endpointByTypes.put(endpoint.getClass(), endpoint);

        //Tạo multithreading mỗi thread xử lý 1 message của JMS với endpoint
        int threadPoolSize = setting.getThreadPoolSize();
        if (threadPoolSize > 0) {
            if (!(endpoint instanceof Runnable))
                throw new IllegalArgumentException("if you want to start thread on endpoint, plz implements Runnable");
            ThreadFactory threadFactory = new NThreadFactory("jms-endpoint-" + setting.getId());
            ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize, threadFactory);
            for (int i = 0; i < threadPoolSize; ++i)
                executorService.execute((Runnable) endpoint);
        }
    }

    public <T> T getEndpoint(String id) {
        return (T) endpointByIds.get(id);
    }

    public <T> T getEndpoint(Class<T> type) {
        return (T) endpointByTypes.get(type);
    }

}
