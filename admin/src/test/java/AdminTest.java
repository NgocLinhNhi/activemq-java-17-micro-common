import heaven.common.jms.JMSConnectionFactoryProxy;
import heaven.system.admin.AdminManager;
import heaven.system.admin.jms.ProductMessageTopicPublisher;
import heaven.system.admin.message.entity.ProductMessage;
import heaven.system.admin.message.msgimpl.ListParamAdminInfo;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Properties;

public class AdminTest {

    public static void main(String[] args) throws Exception {
        ListParamAdminInfo productMessage = getProductMessage();
        //String message = "Hello Ninh";
        //sendMessage(productMessage, "topic.ProductMessage");
        AdminManager.getInstance().start(); // khỏi cần khởi động AdminManagerStartUp
        publishMessage(productMessage);
        //--> không cần gọi method subscriber -> hệ thống tự nhận mesage khi đã chạy file startUp
        //flow test publish (productMessage -> subscriber ( productMessage) -> publish (producerMessage)
    }

    private static ListParamAdminInfo getProductMessage() {
        ListParamAdminInfo listParamAdminInfo = new ListParamAdminInfo("hello", 1);
        listParamAdminInfo.setProductName("Viet");
        listParamAdminInfo.setAddress("Nha Trang");
        listParamAdminInfo.setAge("30");
        //set up cho design pattern xử lý product - MSG_TYPE_PRODUCT
        listParamAdminInfo.setMessageType(1);
        //listParamAdminInfo.setMessageType(2);
        return listParamAdminInfo;
    }

    //* For Unit-Test publish message - nên phải truyền session và publisher...
    public static void publishMessage(ListParamAdminInfo message) throws JMSException {
        TopicConnection connection = JMSConnectionFactoryProxy.getInstance().createTopicConnection();
        TopicSession topicSession = connection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
        Topic topic = topicSession.createTopic("topic.ProductMessage");
        TopicPublisher publisher = topicSession.createPublisher(topic);
        publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

        Properties pros = new Properties();
        pros.setProperty("id", "ProductMessageTopicPublisher");
        pros.setProperty("name", "topic.ProductMessage");
        //publish message = topic ProductMessageTopicPublisher
        ProductMessageTopicPublisher test = new ProductMessageTopicPublisher(pros);
        test.publish(message, topicSession, publisher);
        connection.start();
    }


    //Test send vu vơ
    public static void sendMessage(final ProductMessage message, final String topicName)
            throws Exception {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                "failover://(tcp://localhost:61616)?initialReconnectDelay=1000&startupMaxReconnectAttempts=5");
        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic topic = session.createTopic(topicName);
        MessageProducer producer = session.createProducer(topic);

        connection.start();
        ObjectMessage m1 = session.createObjectMessage();
        m1.setObject(message);

        producer.send(m1);
        System.out.print("Ok");
    }
}