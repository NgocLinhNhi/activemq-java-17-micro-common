package heaven.system.admin.business.impl;

import heaven.system.admin.business.interfaces.IBusiness;
import heaven.system.admin.handler.ProductUpdateHandler;
import heaven.system.admin.message.entity.ProductMessage;
import heaven.system.admin.message.msgimpl.AdminMessageImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductIBusinessImpl implements IBusiness<ProductMessage> {
    private final Logger logger = LoggerFactory.getLogger(ProductUpdateHandler.class);

    //Method xử lý nghiệp vụ cho step doHandle() của Product
    //Được giấu sâu dưới design pattern - factory method
    @Override
    public int insert(ProductMessage request) {
        try {
            System.out.println("THIS IS  STEP HANDLER FOR PRODUCT ");
            System.out.println("ADDRESS : " + request.getAddress());
        } catch (IllegalArgumentException e) {
            logger.error("insert product has error ", e);
            return AdminMessageImpl.PROCESS_FAIL_STATUS;
        }
        return AdminMessageImpl.PROCESS_SUCCESSFUL_STATUS;
    }

    @Override
    public int update(ProductMessage request) {
        return 0;
    }

    @Override
    public int findById(ProductMessage request) {
        return 0;
    }

}
