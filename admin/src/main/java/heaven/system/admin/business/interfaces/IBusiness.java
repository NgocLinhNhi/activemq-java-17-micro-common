package heaven.system.admin.business.interfaces;

public interface IBusiness<T> {
    int insert(T request);

    int update(T request);

    int findById(T request);
}
