package heaven.system.admin;

public class AdminManagerStartup {

    public static void main(String[] args) {
        AdminManager.getInstance().start();
    }
}