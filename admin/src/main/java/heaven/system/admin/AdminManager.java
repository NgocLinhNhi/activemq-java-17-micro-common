package heaven.system.admin;

import heaven.common.jms.JMSClientProxy;
import heaven.common.jms.setting.JMSPublisherSetting;
import heaven.common.jms.setting.JMSSettings;
import heaven.common.jms.setting.JMSSubscriberSetting;
import heaven.system.admin.jms.ManageMessageTopicSubscriber;
import heaven.system.admin.jms.ProducerMessageTopicPublisher;
import heaven.system.admin.jms.ProductMessageTopicPublisher;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdminManager {
   private final Logger logger = LoggerFactory.getLogger(AdminManager.class);

    @Getter
    private JMSClientProxy jmsClientProxy;
    private static final AdminManager INSTANCE = new AdminManager();

    private AdminManager() {
    }

    public static AdminManager getInstance() {
        return INSTANCE;
    }

    //start module create JMS connection to config in Service CommonJMS
    public void start() {
        try {
            logger.info("Starting Admin Manager...");
            startJms();
            logger.info("\nSTART Admin Manager SUCCESS\n");
        } catch (Exception e) {
            logger.error("start Admin Manager exception ", e);
            throw new IllegalStateException(e);
        }
    }

    private void startJms() {
        //Add tất cả endpoint của topic và Queue có trong project vào setting khi khởi tạo JMS
        //Builder pattern here
        JMSSettings settings = (new JMSSettings())
                .addEndpointSetting((new JMSSubscriberSetting())
                        .id("ManageMessageTopicSubscriber")
                        .name("topic.ProductMessage")
                        .supplier(ManageMessageTopicSubscriber::new))
                .addEndpointSetting((new JMSPublisherSetting())
                        .id("ProductMessageTopicPublisher")
                        .name("topic.ProductMessage")
                        .supplier(ProductMessageTopicPublisher::new))
                .addEndpointSetting((new JMSPublisherSetting())
                        .id("ProducerMessageTopicPublisher")
                        .name("topic.ProducerMessage")
                        .supplier(ProducerMessageTopicPublisher::new));
        this.jmsClientProxy = new JMSClientProxy(settings);
        this.jmsClientProxy.start();
    }

}
