package heaven.system.admin.message.msginterface;

import java.io.Serializable;

public interface AdminMessage extends Serializable {

	String getTarget();

	int getCommandType();

	boolean isShutdownCommand();

	boolean isStartCommand();

	boolean  isStopCommand();

}
