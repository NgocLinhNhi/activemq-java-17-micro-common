package heaven.system.admin.message.msgimpl;

import heaven.system.admin.message.msginterface.AdminMessage;

import java.io.Serial;

public class AdminMessageImpl implements AdminMessage {
    @Serial
    private static final long serialVersionUID = 1L;

    private final String m_target;
    private final int m_commandType;

    public static final int COMMAND_TYPE_SHUTDOWN = 0;
    public static final int COMMAND_TYPE_START = 1;
    public static final int COMMAND_TYPE_STOP = 2;

    public static final int PROCESS_SUCCESSFUL_STATUS = 1;
    public static final int PROCESS_FAIL_STATUS = 0;

    public AdminMessageImpl(String target, int commandType) {
        this.m_target = target;
        this.m_commandType = commandType;
    }

    @Override
    public String getTarget() {
        return m_target;
    }

    @Override
    public int getCommandType() {
        return m_commandType;
    }

    @Override
    public boolean isShutdownCommand() {
        return m_commandType == COMMAND_TYPE_SHUTDOWN;
    }

    @Override
    public boolean isStartCommand() {
        return m_commandType == COMMAND_TYPE_START;
    }

    @Override
    public boolean isStopCommand() {
        return m_commandType == COMMAND_TYPE_STOP;
    }
}
