package heaven.system.admin.message.msgimpl;

import heaven.system.admin.message.entity.ProductMessage;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListParamAdminInfo extends ProductMessage {

    public static final int MSG_TYPE_PRODUCT = 1;
    public static final int MSG_TYPE_PRODUCER= 2;

    // set xử lý của Factory Pattern 1 = Product  || 2= producer
    private int messageType;
    private List<ListParamAdminInfo> listParamAdminInfos;

    public ListParamAdminInfo(String target, int commandType) {
        super(target, commandType);
    }
}
