package heaven.system.admin.factory.method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class RequestHandler<T> {
    //Template Method Design Pattern !!!
    //Tạo các method Abstract để các Class kế thừa có các step xử lý riêng :
    //preHandle - doHandle - postHandle
    private Logger logger = LoggerFactory.getLogger(RequestHandler.class);

    public final void handle(T request) {
        try {
            preHandle(request);
            doHandle(request);
            postHandle(request);
        } catch (Exception e) {
            logger.error("handle request: {}, ", request, e);
        }
    }

    protected void preHandle(T request) {
    }

    protected void postHandle(T request) {
    }

    //abstract để bắt buộc class con phải implements xử lý nghiệp vụ here
    //preHandle or postHandle có thể có business không cần => không abstract
    protected abstract void doHandle(T request) throws Exception;
}
