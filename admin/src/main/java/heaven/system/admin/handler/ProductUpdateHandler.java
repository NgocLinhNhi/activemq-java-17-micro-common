package heaven.system.admin.handler;

import heaven.system.admin.enums.BusinessTypeEnum;
import heaven.system.admin.factory.method.BusinessFactory;
import heaven.system.admin.message.entity.ProductMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductUpdateHandler extends ManagerRequestHandler<ProductMessage> {

    private final Logger logger = LoggerFactory.getLogger(ProductUpdateHandler.class);

    //Extend ManagerRequestHandler có luôn steps xử lý của RequestHandler
    // với các method preHandle + doHandle + PostHandle
    @Override
    protected void doHandle(ProductMessage request) {
        handleRateBet(request);
        //Publish Message cho 1 Queue/Topic khác nữa là  ProducerMessageTopicPublisher
        //extends ManagerRequestHandler để có thể gọi thẳng method publishProducerMessage !!!
        publishProducerMessage(request);
    }

    @Override
    protected void preHandle(ProductMessage request) {
        System.out.println("BEFORE HANDLE FOR PRODUCT REQUEST {}" + request);
        logger.info("START HANDLE FOR PRODUCT REQUEST {}", request);
    }

    @Override
    protected void postHandle(ProductMessage request) {
        System.out.println("END HANDLE REQUEST FOR PRODUCT {}" + request);
        logger.info("END HANDLE REQUEST FOR PRODUCT {}", request.getAge());
    }

    //Goi method xử lý nghiệp vụ cho product
    private void handleRateBet(ProductMessage request) {
        //java 10 var type
        var factory = BusinessFactory.getInstance().getBusiness(BusinessTypeEnum.PRODUCT);
        factory.insert(request);
    }
}
