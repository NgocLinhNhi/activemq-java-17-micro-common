package heaven.system.admin.handler;

import heaven.system.admin.business.interfaces.IBusiness;
import heaven.system.admin.enums.BusinessTypeEnum;
import heaven.system.admin.factory.method.BusinessFactory;
import heaven.system.admin.message.entity.ProductMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProducerUpdateHandler extends ManagerRequestHandler<ProductMessage> {

    private final Logger logger = LoggerFactory.getLogger(ProducerUpdateHandler.class);

    @Override
    protected void doHandle(ProductMessage request) {
        handleRateBet(request);
    }

    @Override
    protected void preHandle(ProductMessage request) {
        System.out.println("start handle for Producer request {}" + request);
        logger.info("start handle for Producer request {}", request);
    }

    @Override
    protected void postHandle(ProductMessage request) {
        System.out.println("end handle request for Producer {}" + request);
        logger.info("end handle request for producer {}", request);
    }

    //Goi method xử lý nghiệp vụ cho producer
    //các đối tượng khởi tạo được che giấu = design pattern - factory method
    private void handleRateBet(ProductMessage request) {
        var factory = BusinessFactory.getInstance().getFactory(BusinessTypeEnum.PRODUCER);
        factory.insert(request);
    }

}
