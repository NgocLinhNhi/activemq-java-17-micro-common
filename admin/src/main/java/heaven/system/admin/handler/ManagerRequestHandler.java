package heaven.system.admin.handler;

import heaven.system.admin.AdminManager;
import heaven.system.admin.factory.method.RequestHandler;
import heaven.system.admin.jms.ProducerMessageTopicPublisher;
import heaven.system.admin.message.entity.ProductMessage;

public abstract class ManagerRequestHandler<T> extends RequestHandler<T> {

    //sau khi subscriber message từ class publishTest lại publish cho 1 Topic khác (topic.producer)
    protected void publishProducerMessage(ProductMessage productMessage) {
        ProducerMessageTopicPublisher producerMessagePublisher = AdminManager.getInstance()
                .getJmsClientProxy()
                .getEndpoint(ProducerMessageTopicPublisher.class);
        producerMessagePublisher.publish(productMessage);
    }

}
