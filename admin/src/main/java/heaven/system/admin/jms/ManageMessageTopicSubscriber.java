package heaven.system.admin.jms;

import heaven.common.jms.receiver.JMSTopicSubscriber;
import heaven.system.admin.factory.method.MessageFactory;
import heaven.system.admin.handler.ManagerRequestHandler;
import heaven.system.admin.message.msgimpl.ListParamAdminInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import java.util.Map;
import java.util.Properties;

public class ManageMessageTopicSubscriber extends JMSTopicSubscriber {
    private final Logger logger = LoggerFactory.getLogger(ManageMessageTopicSubscriber.class);

    //use design pattern factory method to manage handler multiple request
    //gọi đến handler nào thì sẽ xử lý handler đấy (Product/producer)
    @SuppressWarnings("rawtypes")
    private final Map<Integer, ManagerRequestHandler> handlers;

    public ManageMessageTopicSubscriber(Properties properties) {
        super(properties);
        handlers = MessageFactory.getInstance().getHandlers();
    }

    @Override
    public void subscribeMsg() {
        try {
            Message originMsg = subscriber.receive();
            if (originMsg instanceof ObjectMessage objectMessage) {
                logger.info("THIS IS OBJECT MESSAGE HAS BEEN RECEIVED !!! ");
                //Java 16 instanceOf pattern
                if (objectMessage.getObject() instanceof ListParamAdminInfo message) {
                    var handler = handlers.get(message.getMessageType());
                    if (handler == null)
                        throw new IllegalStateException("has no handler for message type: " + message.getMessageType());
                    handler.handle(message);
                }
            } else if (originMsg instanceof TextMessage textMessage) {
                System.out.println("Message Accept: " + textMessage.getText());
            } else {
                logger.info("unsupported handle message: {}", originMsg);
            }
        } catch (Exception e) {
            logger.error("parse admin message ", e);
        }
    }

    //classic no design pattern for subscriber message
//    @Override
//    public void subscribeMsg() {
//        try {
//            Message originMsg = subscriber.receive();
//            if (originMsg instanceof ObjectMessage) {
//                ObjectMessage objectMessage = (ObjectMessage) originMsg;
//                ProductMessage customerMessage = (ProductMessage) objectMessage.getObject();
//                System.out.println("Message Get from publisher : " + customerMessage.getProductName());
//            } else if (originMsg instanceof TextMessage) {
//                TextMessage textMessage = (TextMessage) originMsg;
//                System.out.println("Message Accept: " + textMessage.getText());
//            } else {
//                logger.info("unsupported handle message: {}", originMsg);
//            }
//        } catch (Exception e) {
//            logger.error("parse admin message ", e);
//        }
//    }

}
