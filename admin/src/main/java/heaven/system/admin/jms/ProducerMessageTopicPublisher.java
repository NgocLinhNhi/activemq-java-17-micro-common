package heaven.system.admin.jms;

import heaven.common.jms.sender.JMSTopicPublisher;
import heaven.system.admin.message.entity.ProductMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import java.util.Properties;

public class ProducerMessageTopicPublisher extends JMSTopicPublisher {
    private final Logger logger = LoggerFactory.getLogger(ProducerMessageTopicPublisher.class);

    public ProducerMessageTopicPublisher(Properties properties) {
        super(properties);
    }

    public void publish(ProductMessage productMsg) {
        ObjectMessage objMsg;
        try {
            objMsg = session.createObjectMessage();
            objMsg.setObject(productMsg);
            System.out.println("Producer Message has been sent !!!: " + productMsg.getProductName());
            publisher.send(objMsg);
        } catch (JMSException e) {
            logger.error("publish admin message: {}, ", productMsg, e);
        }
    }
}
