package heaven.system.admin.jms;

import heaven.common.jms.sender.JMSTopicPublisher;
import heaven.system.admin.message.msgimpl.ListParamAdminInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TopicPublisher;
import java.util.Properties;

public class ProductMessageTopicPublisher extends JMSTopicPublisher {
    private final Logger logger = LoggerFactory.getLogger(ProductMessageTopicPublisher.class);

    public ProductMessageTopicPublisher(Properties properties) {
        super(properties);
    }

    /**
     * For Unit-Test publish message - nên phải truyền session và publisher...
     */
    public void publish(ListParamAdminInfo productMsg, Session session, TopicPublisher publisher) {
        ObjectMessage objMsg;
        try {
            objMsg = session.createObjectMessage();
            System.out.println("Publisher Product Message: " + productMsg.getProductName());
            objMsg.setObject(productMsg);
            publisher.send(objMsg);
        } catch (JMSException e) {
            logger.error("publish admin message: {}, ", productMsg, e);
        }
    }

//    //for run app
//    public void publish(ListParamAdminInfo productMsg) {
//        ObjectMessage objMsg;
//        try {
//            objMsg = session.createObjectMessage();
//            System.out.println("Publisher Product Message: " + productMsg.getProductName());
//            objMsg.setObject(productMsg);
//            publisher.send(objMsg);
//        } catch (JMSException e) {
//            logger.error("publish admin message: {}, ", productMsg, e);
//        }
//    }
}
