package heaven.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivemqJava17MicroCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivemqJava17MicroCommonApplication.class, args);
    }

}
